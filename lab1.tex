\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 1 - Head-to-Tail Method of Vector Addition}
\def\mydate{September 2, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab demonstrates the concept of translational equilibrium by requiring students to manually add three force vectors using head-to-tail vector addition with a ruler and protractor, in order to find the equilibrium force vector of the system.

\subsubsection*{II. Procedures and Descriptions}

On the instruction sheet for Virtual Lab 01, three force vectors labelled $\vec{A}$, $\vec{B}$, and $\vec{C}$ are given on an xy-coordinate plane using a scale of 1 cm = 10 Newtons. Vector $\vec{A}$ has magnitude 100.0 N and is oriented $30^{\circ}$ CCW from the positive x-axis, vector $\vec{B}$ has magnitude 80.0N and is oriented $30^{\circ}$ CCW from the positive y-axis, and vector $\vec{C}$ has magnitude 40.0 N and is oriented $53^{\circ}$ CCW from the negative x-axis. We are asked to use a ruler and protractor and the method of head-to-tail vector addition to find the sum of the three vectors, then measure its magnitude and angle.

Due to limits of space on the page, we will work backwards and add vector $\vec{B}$ to vector $\vec{C}$, and then add vector $\vec{A}$ to the result of $\vec{C} + \vec{B}$. To add vectors using the head-to-tail method, we first measure the angle $\theta$ between vectors $\vec{C}$ and $\vec{B}$, then subtract this value from $180^{\circ}$ to give us a new value, $\theta'$. (In this case $\theta = 113^{\circ}$ and $\theta' = 67^{\circ}$.) We then use the protractor to measure an angle of $\theta'$ degrees CCW from the head of vector $\vec{C}$, and trace out a line along this angle. Next, we use the ruler to draw a line of the same length as $\vec{B}$ (i.e. having magnitude $|\vec{B}|$) at the measured angle starting from the head of $\vec{C}$. We then repeat this process in order to add the vector $\vec{A}$, but this time starting from the line we drew from the head of $\vec{C}$. (This is easier because the angle between $\vec{A}$ and $\vec{B}$ is easily seen to be $90^{\circ}$ from the values given, and a protractor is not needed to draw it.)

Finally, to find the sum vector $\vec{A} + \vec{B} + \vec{C}$, we draw a straight line from the origin to the end of the line we just drew, and place an arrow at that end indicating the vector's direction. Measuring the vector's length gives a value of about 9.2 cm (meaning the vector has a magnitude of 92 N), and its measured angle from the positive y-axis is $10^{\circ}$ CW.

\subsubsection*{III. Data}

All angles are given counter-clockwise (CCW) relative to the positive x-axis.

\begin{tabular}
	{|c|c|c|}
	\hline
	\textbf{Vector} & \textbf{Magnitude} & \textbf{Angle} \\
	\hline
	$\vec{A}$ & 100.0 N & $30^{\circ}$ \\
	\hline
	$\vec{B}$ & 80.0 N & $120^{\circ}$ \\
	\hline
	$\vec{C}$ & 40.0 N & $233^{\circ}$ \\
	\hline
	$\vec{A} + \vec{B} + \vec{C}$ & 92.0 N & $80^{\circ}$ \\
	\hline
\end{tabular}

\newpage

\subsubsection*{IV. Figures}

\begin{figure}[h!]
	\includegraphics[width=\linewidth]{lab1_fig1.jpg}
	\caption{The three vectors and their sum, drawn using the head-to-tail method}
\end{figure}

\subsubsection*{V. Analysis}

Using the values measured above we can estimate that the sum of the three force vectors would have a magnitude of about 92 N, and an angle of $80^{\circ}$ CCW from the positive x-axis.

Using the equations $|\vec{R}_x| = |\vec{R}| \cos(\theta)$ and $|\vec{R}_y| = |\vec{R}| \sin(\theta)$, we can calculate the $x$ and $y$ components of the three force vectors and add them algebraically to get an exact value for the magnitude and angle of the sum vector, and thereby determine the error margin in the vector drawn using the head-to-tail procedure. Doing this, we find that that $\vec{A} \approx \langle 86.6,50.0 \rangle$, $\vec{B} \approx \langle -40.0,69.3 \rangle$, and $\vec{C} \approx \langle -24.1,-31.9 \rangle$. To find the sum (which we will refer to as $\vec{V}$), we simply add the $x$ and $y$ components of the three vectors together, which gives us that $\vec{A} + \vec{B} + \vec{C} = \vec{V} \approx \langle 22.5,87.3 \rangle$. We can then calculate the magnitude and angle of $\vec{V}$ using the following equations:
\begin{flalign*}
	  |\vec{V}| &= \sqrt{|\vec{V}_x|^2 + |\vec{V}_y|^2} = \sqrt{(22.5)^2 + (87.3)^2} \approx 90.2 \ \text{N} \\
	\theta_{\vec{V}} &= \arctan\left(\frac{|\vec{V}_y|}{|\vec{V}_x|}\right) = \arctan\left(\frac{87.3}{22.5}\right) \approx 75.5^{\circ} &
\end{flalign*}

Therefore, we can deduce that the actual magnitude of the resultant vector to be 90.2 N, and have an angle of $75.5^{\circ}$ CCW of the positive x-axis.

We will then use this information to calculate the percent error in our hand-drawn measurements. Recall that our hand-drawn sum vector had a magnitude of 92 N and an angle of $80^{\circ}$. When we subtract 90.2 N and $75.5^{\circ}$ from these respective values, we find an error of 1.8 N for the magnitude, and and error of $4.5^{\circ}$ for the angle. This gives us an error percentage of 1.96\% for the magnitude and 5.63\% for the angle.

\subsubsection*{VI. Conclusions}

In order to use the values measured for our sum vector to estimate the translational equilibrium of the three forces, we simply reverse its direction so that it cancels out the other three vectors. Thus, the equilibrium force vector would still have a magnitude of 92 N, but would have an angle of $260^{\circ}$ CCW relative to the positive x-axis, or $10^{\circ}$ CW relative to the negative y-axis.

\end{flushleft}
\end{document}
