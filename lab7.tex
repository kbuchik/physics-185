\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 07 - Collisions}
\def\mydate{November 6, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}

% Data plot packages
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab uses two PhysLets applets which simulate the collision of two carts under different conditions: the first to examine whether four collisions were elastic or inelastic based on the initial and final momentum and kinetic energy of the system, and the second to analyze the linear momentum of the carts after a collision when a spring is involved.

\subsubsection*{II. Procedures and Descriptions}

Part I of the procedure uses PhysLets problem 8.6, which presents us with four animations, one each simulating two carts, all of mass 1 kg. The carts either move toward each other at constant velocities, or one moves at a constant velocity with the other being stationary, before colliding and moving apart. For each animation, we calculate and record the linear momentum of each cart before and after the collision (according to the formula $\vec{p} = m\vec{v}$, although since the masses are all 1 kg the momentum is always the same as the velocity), and the kinetic energy of each cart before and after the collisions (according to the formula ${\displaystyle KE = \frac{mv^2}{2}}$). We then add the values for each cart together to get the initial and final momentum and kinetic energy of the whole system. Comparing the initial and final values will allow us to determine whether linear momentum was conserved, and whether each collision was elastic or inelastic.

In Part II, we use PhysLets problem 8.8, which simulates an "explosive" collision, so called because it involves the sudden release of a spring. In this simulation, a spring is attached to one cart and in contact with a second cart, and is initially compressed from its natural length. At $t = 0.22$ seconds, the spring releases and the carts begin moving away from each other at constant velocities. We are asked to calculate those velocities based on the position of the carts at different points in time, and then use that information to calculate the total change in linear momentum due to the release of the spring.

\subsubsection*{III. Data}

\underline{\textbf{Animation \#1:}}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0$ [$J$] & $KE_f$ [$J$] \\
	\hline
	Cart 1 & -3 & +3 & 4.5 & 4.5 \\
	\hline
	Cart 2 & +3 & -3 & 4.5 & 4.5 \\
	\hline
\end{tabular}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0^{total}$ [$J$] & $KE_f^{total}$ [$J$] \\
	\hline
	Cart 1 + Cart 2 (system) & 0 & 0 & 9 & 9 \\
	\hline
\end{tabular}

\underline{\textbf{Animation \#2:}}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0$ [$J$] & $KE_f$ [$J$] \\
	\hline
	Cart 1 & -6 & 0 & 18 & 0 \\
	\hline
	Cart 2 & 0 & -6 & 0 & 18 \\
	\hline
\end{tabular}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0^{total}$ [$J$] & $KE_f^{total}$ [$J$] \\
	\hline
	Cart 1 + Cart 2 (system) & -6 & -6 & 18 & 18 \\
	\hline
\end{tabular}

\newpage

\underline{\textbf{Animation \#3:}}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0$ [$J$] & $KE_f$ [$J$] \\
	\hline
	Cart 1 & -1.5 & +4.5 & 1.125 & 10.125 \\
	\hline
	Cart 2 & +4.5 & -1.5 & 10.125 & 1.125 \\
	\hline
\end{tabular}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0^{total}$ [$J$] & $KE_f^{total}$ [$J$] \\
	\hline
	Cart 1 + Cart 2 (system) & +3 & +3 & 11.25 & 11.25 \\
	\hline
\end{tabular}

\underline{\textbf{Animation \#4:}}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0$ [$J$] & $KE_f$ [$J$] \\
	\hline
	Cart 1 & -2 & +4 & 2 & 8 \\
	\hline
	Cart 2 & +4 & -1 & 8 & 0.5 \\
	\hline
\end{tabular}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	\ & $\vec{p}_0^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_f^{\ total}$ $\left[kg \cdot \frac{m}{s}\right]$ & $KE_0^{total}$ [$J$] & $KE_f^{total}$ [$J$] \\
	\hline
	Cart 1 + Cart 2 (system) & +2 & +3 & 10 & 8.5 \\
	\hline
\end{tabular}

\underline{\textbf{Explosive Collision:}}

(I know a data table was not part of the lab for this section, but I'm adding it to make collecting data easier)

$m_1 = 1.35kg;\ m_2 = 0.90kg$

\begin{tabular}{|c|c|c|c|c|c|c|}
	\hline
	$t$ [$s$] & $x_1$ [$m$] & $x_2$ [$m$] & $\vec{v}_1$ [$m/s$] & $\vec{v}_2$ [$m/s$] & $\vec{p}_1 \left[kg \cdot \frac{m}{s}\right]$ & $\vec{p}_2 \left[kg \cdot \frac{m}{s}\right]$ \\
	\hline
	0 & -0.3 & +0.3 & 0 & 0 & 0 & 0 \\
	\hline
	0.22 & -0.42 & +0.43 & 0 & 0 & 0 & 0 \\
	\hline
	0.32 & -0.52 & +0.58 & -1 & 1.5 & -1.35 & 1.35 \\
	\hline
	0.42 & -0.62 & +0.73 & -1 & 1.5 & -1.35 & 1.35 \\
	\hline
\end{tabular}

\newpage

\subsubsection*{IV. Figures}

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[ht!]
\begin{center}
	\includegraphics[scale=1]{lab7_fig1.jpg}
\end{center}
\end{figure}

\subsubsection*{V. Analysis}

In the cases of animations 1, 2, and 3, the total linear momentum was conserved because the values of $\vec{p}_{total}$ were the same before and after the collisions. In the case of animation 4, the linear momentum was not conserved, because in this case $\Delta \vec{p}_{total} = 1$.

The collisions in animations 1, 2, and 3 were all elastic because the kinetic energy ($KE_{total})$ was the same before and after each collision, which is what defines an elastic collision. The collision in animation 4, however, was an inelastic collision, because $\Delta KE_{total} = -1.5$, and since the total kinetic energy of the system changed based on the collision, the collision was thus inelastic.

For the explosive collision, immediately after the spring was released the velocity of both carts would have been zero, but once they started moving, $\vec{v}_1 = -1$ m/s (or 1 m/s to the left) and $\vec{v}_2 = 1.5$ m/s (or 1 m/s to the right). The linear momentum values of the carts after the release of the spring (at $t > 0.22$ seconds) are $\vec{p}_1 = -1.35$ and $\vec{p}_2 = 1.35$ respectively. The change in the net linear momentum of the system due to the spring was $\Delta \vec{p} = 0$ kg m/s, meaning that linear momentum was conserved.

\subsubsection*{VI. Conclusions}

We can tell that the first three collisions were elastic, because linear momentum and kinetic energy were the same before and after the collision. In the fourth collision, the values were different, meaning the collision was inelastic and linear momentum was NOT conserved; however in order to obey conservation laws, we know that the "missing" 1.5 J of kinetic energy must have been converted into another form, possibly heat. In the case of the explosive collision, due to the respective masses and velocities of the two carts, the linear momentum vectors after the spring release work out to have the same magnitude but acting in opposite directions, meaning that the linear momentum in this event was also conserved.

\end{flushleft}
\end{document}
