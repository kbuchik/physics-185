\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 08 - Torque and Moment of Inertia}
\def\mydate{November 17, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}
% Tweak table cell height
\renewcommand{\arraystretch}{0.8}

% Data plot packages
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}

% Lab 8 data
\begin{filecontents*}{lab8_data.csv}
t,w,v
0.0,0,0
0.2,0.49,0.98
0.4,0.98,1.96
0.6,1.47,2.94
0.8,1.96,3.92
1.0,2.45,4.9
1.2,2.94,5.88
1.4,3.43,6.86
1.6,3.92,7.84
1.8,4.41,8.82
2.0,4.9,9.8
2.2,5.39,10.78
2.4,5.88,11.76
2.6,6.37,12.74
2.8,6.86,13.72
\end{filecontents*}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab uses a simulated Atwood machine (a hanging block attached to a rope wrapped around a circular pulley) to demonstrate how Newton's second law for rotation can be used to calculate the linear and angular acceleration of the system, given measurements of the pulley's angular velocity.

\subsubsection*{II. Procedures and Descriptions}

PhysLets Exploration 10.3 simulates a simple Atwood machine, in the form of a single pulley rotating on a fixed axis, with a single block hanging from a rope which is wrapped around the pulley. The mass of the block ($m_1$) is given as 1 kg, the mass of the pulley ($m_2$) as 2 kg, and the radius of the pulley ($R$) as 2 m. Time can then be moved forwards and backwards, and the current value of the pulley's angular velocity ($\omega$) will be given. We record these values of $\omega$ in a table starting from $t = 0$ s to $t = 2.8$ s, in 0.2 second intervals (seen in Section III). For each entry, we then calculate and record the current linear velocity of the block, based on the the equation $v = \omega R$.

\newpage

\subsubsection*{III. Data}

$m_1 = 1.00$ kg, $m_2 = 2.00$ kg, $R = 2.00$ m

\begin{tabular}{|c|c|c|}
	\hline
	$t$ [s] & $\omega$ [rad/s] & $v = \omega R$ [m/s]
	\csvreader[head to column names]{lab8_data.csv}{}
	{\\\hline\t & \w & \v}
	\\\hline
\end{tabular}

\newpage

\subsubsection*{IV. Figures}

\underline{\textbf{Linear speed:}}

\begin{tikzpicture}
	% Grid plot setup
	\begin{axis}[xmin = 0, xmax = 3, ymin = 0, ymax = 15, width = \textwidth, height = 0.75\textwidth, xtick distance = 0.2, ytick distance = 1, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Time ($t$) [s]}, ylabel = {Block speed ($v$) [m/s]}, legend cell align = {left}, legend pos = north west]
	% Plot data from lab2_data.csv
	\addplot[red, only marks] table[x=t, y=v, col sep=comma]{lab8_data.csv};
	% Draw linear regression
	\addplot[thick, blue] table[x=t, y={create col/linear regression={y=v}}, col sep=comma]{lab8_data.csv};
	% Add legend
	\addlegendentry{\ $t$ vs $v$}
	\addlegendentry{Linear regression: $v = \pgfmathprintnumber{\pgfplotstableregressiona} t$};
\end{axis}
\end{tikzpicture}

\newpage

\underline{\textbf{Angular speed:}}

\begin{tikzpicture}
	% Grid plot setup
	\begin{axis}[xmin = 0, xmax = 3, ymin = 0, ymax = 7, width = \textwidth, height = 0.75\textwidth, xtick distance = 0.2, ytick distance = 0.5, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Time ($t$) [s]}, ylabel = {Pulley speed ($v$) [rad/s]}, legend cell align = {left}, legend pos = north west]
	% Plot data from lab2_data.csv
	\addplot[red, only marks] table[x=t, y=w, col sep=comma]{lab8_data.csv};
	% Draw linear regression
	\addplot[thick, blue] table[x=t, y={create col/linear regression={y=w}}, col sep=comma]{lab8_data.csv};
	% Add legend
	\addlegendentry{\ $t$ vs $\omega$}
	\addlegendentry{Linear regression: $\omega = \pgfmathprintnumber{\pgfplotstableregressiona} t$};
\end{axis}
\end{tikzpicture}

\newpage

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[ht!]
\begin{center}
	\includegraphics[scale=0.8]{lab8_fig1.jpg}
\end{center}
\end{figure}

\subsubsection*{V. Analysis}

The linear acceleration of the hanging block can be determined by running a linear fit on the graphed values of the linear velocity plotted against time, which gives us a value of ${\displaystyle a_{exp} = 4.9}$\ [m/s$^2$]. Because the calculated values for $v$ conform perfectly to a straight line, the linear fit will give us the same result as using any two of the points to calculate the slope. Since acceleration is defined as the derivative of a velocity function, we know that the slope of the velocity graph equals the vertical acceleration of the box.

Using Newton's second law of motion for translation ($\Sigma \vec{F} = m \vec{a}$), we can calulate a theoretical value for the linear acceleration based on the masses of the box and pulley, and the acceleration due to gravity:

${\displaystyle a_{theory} = \frac{2m_1 g}{2m_1 + m_2} = \frac{2(1)(9.8)}{2(1) + 2} = \frac{19.6}{4} = 4.9}$ [m/s$^2$]

Lastly, the percent error is calculated as follows, which (unsurprisingly) works out to zero:

\% error = ${\displaystyle 100 \left(\frac{a_{exp} - a_{theory}}{a_{theory}}\right) = 100 \left(\frac{4.9 - 4.9}{4.9}\right) = 0\%}$

Similarly, we can run a linear fit of the rotational values values vs. time to get an experimental value for the angular acceleration, which is  ${\displaystyle \alpha_{exp} = 2.45}$ [rad/s$^2$]. These points also conform to a straight line, so they're going to match the theoretical value calculated by using Newton's second law of motion for rotation ($\Sigma \vec{\tau} = I \vec{\alpha}$):

${\displaystyle \alpha_{theory} = \frac{2m_1 g}{R(2m_1 + m_2)} = \frac{2(1)(9.8)}{2(2(1) + 2)} = \frac{9.8}{4} = 2.45}$ [rad/s$^2$]

And of course, the error percentage is once again zero:

\% error = ${\displaystyle 100 \left(\frac{\alpha_{exp} - \alpha_{theory}}{\alpha_{theory}}\right) = 100 \left(\frac{2.45 - 2.45}{2.45}\right) = 0\%}$

\subsubsection*{VI. Conclusions}

If this experiment were run using a real Atwood machine instead of a simulated one, variables such as friction on the pulley, imperfections in the rope, air resistance, etc. would have affected the experiment, and the velocity values would not have resulted in a straight line when plotted, and thus the linear fits would not give results perfectly matching the theoretical values for $a$ and $\alpha$.

\end{flushleft}
\end{document}
