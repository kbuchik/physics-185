\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 05 - Uniformly Accelerated Motion in 1-Dimension}
\def\mydate{October 15, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}
% Tweak table cell height
\renewcommand{\arraystretch}{0.8}

% Data plot packages
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}

% Lab 5 data
\begin{filecontents*}{lab5_data.csv}
t,tsquared,xinitial,xfinal,deltax
0,0,-2.00,-2.00,0
0.1,0.01,-2.00,-1.98,0.02
0.2,0.04,-2.00,-1.93,0.07
0.3,0.09,-2.00,-1.87,0.13
0.4,0.16,-2.00,-1.73,0.27
0.5,0.25,-2.00,-1.59,0.41
0.6,0.36,-2.00,-1.43,0.57
0.7,0.49,-2.00,-1.20,0.80
0.8,0.64,-2.00,-0.96,1.04
\end{filecontents*}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab uses a PhysLets applet simulating a modified Atwood machine to demonstrate how acceleration can be calculated by doing a linear fit of an object's displacement versus time squared, and how the acceleration can be used in combination with Newton's laws to calculate other values such as the tension force of the rope, and the mass of the other object in the system.

\subsubsection*{II. Procedures and Descriptions}

PhysLets problem 4.11 simulates a \textit{modified Atwood machine}, which consists of a 1 kg cart sitting on a surface, attached to a string which hangs on a pulley over the edge of the surface, which is attached to a hanging object of unknown mass at the other end. Both the surface and the pulley are assumed to be frictionless. The data in this experiment consists of measurements of the horizontal position of the cart ($x_f$) taken in intervals of 0.1 seconds from $t = 0$ to $t = 0.8$, from a starting position of $x_0 = -2.0$ m. Once measurements are taken, the horizontal displacement values (denoted by $\Delta x$) are plotted on the y-axis against the time-squared values on the x-axis. A linear regression is then run on this data set, which yields a best-fit line with the equation $\Delta x = 1.62t$. This can then be used to estimate the horizontal acceleration of the cart, which is detailed in the Analysis section below.

\subsubsection*{III. Data}

\begin{tabular}{|c|c|c|c|c|}
	\hline
	$t$ [s] & $t^2$ [$\text{s}^2$] & $x_0$ [m] & $x_f$ [m] & $\Delta x = (x_f - x_0)$ [m]
	\csvreader[head to column names]{lab5_data.csv}{}
	{\\\hline\t & \tsquared & \xinitial & \xfinal & \deltax}
	\\\hline
\end{tabular}

\newpage

\subsubsection*{IV. Figures}

\begin{tikzpicture}
	% Grid plot setup
	\begin{axis}[xmin = 0, xmax = 0.7, ymin = 0, ymax = 1.2, width = \textwidth, height = 0.75\textwidth, xtick distance = 0.1, ytick distance = 0.1, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Time squared ($t^2$) [$\text{sec}^2$]}, ylabel = {Distance moved ($\Delta x$) [m]}, legend cell align = {left}, legend pos = north west]
	% Plot data from lab2_data.csv
	\addplot[red, only marks] table[x=tsquared, y=deltax, col sep=comma]{lab5_data.csv};
	% Draw linear regression
	\addplot[thick, blue] table[x=tsquared, y={create col/linear regression={y=deltax}}, col sep=comma]{lab5_data.csv};
	% Add legend
	\addlegendentry{\ $\Delta x$ vs $t^2$}
	\addlegendentry{Linear regression: $\Delta x = \pgfmathprintnumber{\pgfplotstableregressiona} t$};
\end{axis}
\end{tikzpicture}

\newpage

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[ht!]
\begin{center}
	\includegraphics[scale=1]{lab5_fig1.jpg}
\end{center}
\end{figure}

\newpage

\subsubsection*{V. Analysis}

Given that the kinematic equation for linear motion is ${\displaystyle \Delta x = \frac{1}{2}(a)t^2}$, the slope of the linear regression line should be one half of the acceleration, and so the acceleration of the cart is approximately 3.24 m/$\text{s}^2$ in the positive-x direction. Since we're assuming no friction in the system, the tension in the string is given by the acceleration of the cart multiplied by its mass. The cart has a mass of 1 kg, and so the tension force is simply 3.24 N. In order to find the mass of the hanging object, we can use Newton's second law given by $\Sigma F_y = m(a_y)$. The vertical acceleration of the hanging object must be the same as the horizontal acceleration of the cart, so $a_y = 3.24$ m/$\text{s}^2$. $\Sigma F_y$ is the sum of forces acting on the hanging object, which is the force of gravity minus the tension in the rope. We've already calculated the strength of the tension force, so substituting in, we get an equation of $9.8m - 3.24 = 3.24m$. Solving algebraically for $m$ gives us a value of $m = 0.49$ kg for the mass of the hanging object.

\subsubsection*{VI. Conclusions}

By taking measurements of the cart's position over time and applying Newton's laws, we were able to calculate the unknown values of $a_y = 3.24$ m/$\text{s}^2$ for the acceleration of the system, $T = 3.24$ N for the tension force in the rope, and $m = 0.49$ kg for the mass of the hanging object.

\end{flushleft}
\end{document}
