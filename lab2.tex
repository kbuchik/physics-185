\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 02 - Introduction to PhysLets}
\def\mydate{September 16, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}

% Data plot packages
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}

% Lab 2 data (embedded CSV format)
\usepackage{csvsimple}
\begin{filecontents*}{lab2_data.csv}
t,x
0,0.0
1,1.3
2,2.6
3,3.9
4,5.2
5,6.5
6,7.7
7,8.9
8,10.1
9,11.3
10,13.0
\end{filecontents*}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab uses the PhysLets platform to demonstrate several concepts in basic mechanics, including how to use a vernier caliper to measure the length of an object, and taking samples of an object's position over time and using a linear regression to estimate its velocity.

\subsubsection*{II. Procedures and Descriptions}

Section II introduces a tool used to accurately measure the length of objects, called a \textit{vernier caliper}. This looks like a ruler with an adjustable clamp on one end, with one side of the clamp being a smaller piece with another ruler on it, able to slide along the length of the main ruler. The caliper is capable of measuring an object with an accuracy to the hundredths of a centimeter. To measure an object, the object is placed in the "clamp" and the adjustable slide is moved to come in contact with the object. The measurement is read by finding where the "zero" mark on the adjustable slide falls along the main ruler (which is accurate to a tenth of a centimeter), which gives us the first two decimal places of the measurement. The hundredths place is read by looking at the smaller ruler (marked off in ten tenths of a centimeter), and locating which of its marks lines up perfectly with a mark on the larger ruler (of which there should only be one). The corresponding mark on the smaller ruler gives us the hundredths place of the measurement.

Section II shows an example caliper and asks us to read off the measurement, the answer to which is 1.64 cm. It then has us use the PhysLets website to measure the length of four different objects using a virtual caliper. These measurements are given in the first table of the Data section of this lab report.

Section III of the lab features a car which moves along an x-axis (measured in centimeters) over a period of time $t$ (measured in seconds), which we can advance in increments of $0.1 t$. The PhysLet also allows us to measure the $x,y$ coordinates of the car, and manually enter the $x$ and $t$ values into a table which can then be plotted on a graph. We are asked to take 11 measurements of the car's x-coordinate, starting from $t = 0$ and advancing one second at a time until we reach $t = 10$. Once these measurements are plotted, we run a linear regression on the graph, which will give us a best-fit line through the set of points we plotted.

The measurements of these position/time values can be found in the second table of the Data section of this report. In the Figures section, a screenshot of the final graph from the PhysLets site is shown, along with a separate graph of the plotted points including the linear regession line (calculated and drawn using the \textbf{pgfplots} LaTeX package). According to the PhysLets tool, the slope of the linear regression line is calculated as 1.27 cm, with an x-intercept of $0.04 cm$, and as shown in the legend of the first graph in the Figures section, the regression equation calculated by \textbf{pgfplots} agrees with this.

\newpage

\subsubsection*{III. Data}

Lengths of objects in Problem 1.1:

\begin{tabular}{|c|c|}
	\hline
	Object & Length of object [cm] \\
	\hline
	animation 1 (purple circle) & 0.32 \\
	\hline
	animation 2 (blue rectangle) & 2.46 \\
	\hline
	animation 3 (green object) & 0.68 \\
	\hline
	animation 4 (yellow car) & 1.26 \\
	\hline
\end{tabular}

Position of car in Problem 1.2:

\begin{tabular}{|c|c|}
	\hline
	Time $t$ [s] & Position $x$ [cm]
	\csvreader[head to column names]{lab2_data.csv}{}
	{\\\hline\t & \x}
	\\\hline
\end{tabular}

Slope = $+1.27$ cm, intercept = $+0.04$ cm

Equation: $x(t) = 1.27t + 0.04$

\newpage

\subsubsection*{IV. Figures}
Plot of car position vs. time, with linear regression:

\begin{tikzpicture}
	% Grid plot setup
	\begin{axis}[xmin = 0, xmax = 10, ymin = 0, ymax = 15, width = \textwidth, height = 0.75\textwidth, xtick distance = 1, ytick distance = 1, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Time ($t$) [sec]}, ylabel = {Position ($x$) [cm]}, legend cell align = {left}, legend pos = north west]
	% Plot data from lab2_data.csv
	\addplot[red, only marks] table[x=t, y=x, col sep=comma]{lab2_data.csv};
	% Draw linear regression
	\addplot[thick, blue] table[x=t, y={create col/linear regression={y=x}}, col sep=comma]{lab2_data.csv};
	% Add legend
	\addlegendentry{\ Car position}
	\addlegendentry{Linear regression: $x = \pgfmathprintnumber{\pgfplotstableregressiona} t \pgfmathprintnumber[print sign,fixed,precision=3]{\pgfplotstableregressionb}$};
\end{axis}
\end{tikzpicture}

\newpage

Graph and linear regression from PhysLets site:
\begin{figure}[h!]
\begin{center}
	\includegraphics[scale=1]{lab2_fig1.jpg}
\end{center}
\end{figure}

\subsubsection*{V. Analysis}

(Questions from the lab instructions)

1. \textit{What does the slope of the linear fit represent?}

The slope of the linear regression line described in the Procedures section gives us an estimate of the car's average velocity, which is 1.27 cm/s in the positive x direction.

2. \textit{Describe in words the motion of the car.}

Since the measured points form a nearly straight line, we can assume that the car's velocity is constant and its acceleration is 0. Thus, we can say that the car is moving in the positive x direction (to the right) with a constant speed of about 1.27 cm/s. Mathematically, the equation of the best-fit line also gives us a position function for the car: $x(t) = 1.27t + 0.04$.

\subsubsection*{VI. Conclusions}

The first lab exercise demonstrates how the vernier caliper gives us a convenient way of measuring objects (especially irregularly shaped ones) with more precision than is possible with a ruler.

The second lab exercise shows us that an object's velocity can be estimated by taking measurements of its position over time, also providing a demonstration of how velocity is defined as the derivative of the position function.

\end{flushleft}
\end{document}
