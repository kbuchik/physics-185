\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 03 - Free Fall Acceleration}
\def\mydate{September 24, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}

% Data plot packages
\usepackage{pgfplots}
\usepackage{pgf,tikz}
\usepackage{pstricks-add}
\usepackage{siunitx}
\usepackage{latexsym}
\usepackage{keyval}
\usepackage{ifthen}
\usepackage{moreverb}
\usepackage{gnuplottex}%[miktex]%[shell]
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}

% Lab 3 data (embedded CSV format)
\usepackage{csvsimple}
\begin{filecontents*}{lab3_data.csv}
t,tsquared,y
0,0,80
0.5,0.25,78.775
1,1,75.1
1.5,2.25,68.975
2,4,60.4
2.5,6.25,49.375
3,9,35.9
3.5,12.25,19.975
4,16,1.6
\end{filecontents*}

% Second data file needed to draw quadratic fit (couldn't figure out how to get gnuplots to parse comma-separated text!)
\begin{filecontents*}{lab3_quad.csv}
t	y
0	80
0.5	78.775
1	1
1.5	68.975
2	60.4
2.5	49.375
3	35.9
3.5	19.975
4	1.6
\end{filecontents*}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab guides us through using a virtual PhysLets tool to measure the position of an object in free-fall over time, in order to experimentally determine the acceleration due to gravity near the earth's surface.

\subsubsection*{II. Procedures and Descriptions}

Using a tool found in Section 2.6 of the PhysLets site, nine measurements of the height of a simulated object dropped from 80 meters above the earth's surface were taken at intervals of half a second (with the final measurement being at $t = 4$ s. These height and time measurements were recorded in a table, along with the squared time values which will be used in analysis of the data. Two graphs of the data were then generated (in my case, using the \textbf{pgfplots} and \textbf{gnuplot} LaTeX packages); one plotting the time on the x-axis and height on the y-axis and overlaying a quadratic fit of the data, and the other plotting the squared time on the x-axis and height on the y-axis and overlaying a linear fit of the data. Two experimental values of $g$ (i.e. the vertical acceleration due to gravity) were calculated using the two best-fit lines from each graph, by taking $-2$ times the value of the coefficient of the linear (or $x$) term from the two regression functions, and error percentages were calculated by comparing each experimental value from the known theoretical value of $g = 9.8$.

\newpage

\subsubsection*{III. Data}

Section 2.6: Free fall values

\begin{tabular}{|c|c|c|}
	\hline
	$t$ [s] & $t^2$ [$\text{s}^2$] & $y$ [m]
	\csvreader[head to column names]{lab3_data.csv}{}
	{\\\hline\t & \tsquared & \y}
	\\\hline
\end{tabular}

\newpage

\subsubsection*{IV. Figures}

\begin{tikzpicture}
% Grid plot setup
\begin{axis}[xmin = 0, xmax = 5, ymin = 0, ymax = 100, width = \textwidth, height = 0.75\textwidth, xtick distance = 0.5, ytick distance = 10, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Time ($t$) [sec]}, ylabel = {Height ($y$) [m]}, legend cell align = {left}, legend pos = north east]
	% Plot data from lab3_data.csv
	\addplot[only marks, black] table[x=t, y=y, col sep=comma]{lab3_data.csv};i
	% Draw quadratic regression using gnuplots
	\addplot[no markers, blue] gnuplot [raw gnuplot] {
		% Function to fit
		f(x) = a*x**2 + b*x + c;
		% Set reasonable coefficient starting values
		a = 1; b = 2; c = 3;
		% Source data file
		fit f(x) "lab3_quad.csv" u 1:2 via a,b,c;
		% Specify range to plot
		plot [x=0:4] f(x);
		% Save parameters to file
		set print "lab3_params.dat";
		print a,b,c;
	};
	% Add legend
	\addlegendentry{\ Object height vs. $t$}
	\addlegendentry{\pgfplotstableread{lab3_params.dat}\parameters
		\pgfplotstablegetelem{0}{0}\of\parameters \pgfmathsetmacro\paramA{\pgfplotsretval}
		\pgfplotstablegetelem{0}{1}\of\parameters \pgfmathsetmacro\paramB{\pgfplotsretval}
		\pgfplotstablegetelem{0}{2}\of\parameters \pgfmathsetmacro\paramC{\pgfplotsretval}
		Quadratic fit: $y = \pgfmathprintnumber{\paramA} x^2 \pgfmathprintnumber[print sign]{\paramB} x \pgfmathprintnumber[print sign]{\paramC}$
	}
\end{axis}
\end{tikzpicture}

Measured value of $g$: \quad ${\displaystyle g_{\text{exp}} = 10.64}$ [m/s$^2$]

Percent error of $g$: \quad \textit{error} ${\displaystyle = \left( \frac{g_{\text{exp}} - g_{\text{theory}}}{g_{\text{theory}}}\right) 100 = \left(\frac{10.64- 9.8}{9.8}\right) = 8.6 \%}$

\begin{tikzpicture}
% Grid plot setup
\begin{axis}[xmin = 0, xmax = 16, ymin = 0, ymax = 100, width = \textwidth, height = 0.75\textwidth, xtick distance = 1, ytick distance = 10, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Time squared ($t^2$) [$\text{sec}^2$]}, ylabel = {Height ($y$) [m]}, legend cell align = {left}, legend pos = north east]
	% Plot data from lab3_data.csv
	\addplot[only marks, black] table[x=tsquared, y=y, col sep=comma]{lab3_data.csv};i
	% Draw linear regression
	\addplot[no markers, red] table[x=tsquared, y={create col/linear regression={y=y}}, col sep=comma]{lab3_data.csv};
	% Add legend
	\addlegendentry{\ Object height vs. $t^2$}
		\addlegendentry{Linear fit: $y = \pgfmathprintnumber[fixed, precision=2]{\pgfplotstableregressiona} t \pgfmathprintnumber[print sign, fixed, precision=2]{\pgfplotstableregressionb}$};
\end{axis}
\end{tikzpicture}

Measured value of $g$: \quad ${\displaystyle g_{\text{exp}} = 9.8}$ [m/s$^2$]

Percent error of $g$: \quad \textit{error} ${\displaystyle = \left( \frac{g_{\text{exp}} - g_{\text{theory}}}{g_{\text{theory}}}\right) 100 = \left(\frac{9.8 - 9.8}{9.8}\right) = 0 \%}$

\newpage

\subsubsection*{V. Analysis}

The \textbf{gnuplot} LaTeX package was used to calculate a quadratic regression of the time-vs-height data, with both plotted on a single graph. Similarly, the \textbf{pgfplots} package was used to calculate a linear regression of the height-vs-time-squared data, which were plotted on a second graph. The equation of the linear fit line was determined to be $-4.9t + 80$, while the equation of the quadratic fit function was determined to be $-2.33x^2 - 5.32x + 67.87$. This gives us experimental values for $g$ of $10.64$ m/s$^2$ and 9.8 m/s$^2$, respectively. Assuming an actual theoretical value of $g = 9.8$, we calculate the percent error for the quadratic fit to be 8.6\% and the error for the linear fit to be 0\%.

\subsubsection*{VI. Conclusions}

In the case of this experiment, the linear regression method provided a perfectly accurate measurement of $g$ (unsurprising, since a value of $g = 9.8$ was likely used in programming the simulation), while a quadratic regression was associated with a much higher degree of error.

\end{flushleft}
\end{document}
