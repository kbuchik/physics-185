\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 10 - The Simple Pendulum}
\def\mydate{December 5, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}
% Tweak table cell height
\renewcommand{\arraystretch}{0.8}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab uses a PhysLets applet simulating an idealized simple pendulum to investigate how well the formula for angle in a simple harmonic system predicts the the actual angle of the simulated pendulum, as the initial angle from which the pendulum is released is increased from its resting position.

\subsubsection*{II. Procedures and Descriptions}

PhysLets illustration 16.2 simulates a simple pendulum consisting of a 1.0 kg ball attached to the end of a 15 meter string, which can be moved to different angles (i.e. different offsets of $\theta$ as measured in radians, denoted here as $\theta_{max}$) from its resting position at the negative vertical axis. The applet will then simulate the harmonic motion of the pendulum, telling us the current value angle $\theta$ of the pendulum relative to the vertical axis, in increments of 0.05 seconds.

We are given four values of $\theta_{max}$ for which to run the simulation, which are 0.1, 0.3, 0.7, and 1.2 radians respectively. In each set of measurements we record the value of $\theta$ at $t = 1.5, 3, 5, 6.5, \text{ and } 7.2$ seconds. We then use the equation $\theta = \theta_{max} \cos(\omega t)$ to calculate a theoretical value for $\theta$ at each time period, then calculate the error percentages between the measured and theoretical values.

\newpage

\subsubsection*{III. Data}

$L = 15$ meters, $m = 1.0$ kg

$\theta_{max} = 0.1$ radians:

\begin{tabular}{|c|c|c|c|}
	\hline
	$t$ [sec] & $\theta_{theory}$ [radians] & $\theta_{measured}$ [radians] & \% error \\
	\hline
	1.5 & 0.04 & 0.04 & 0.0 \\
	\hline
	3 & -0.08 & -0.08 & 0.0 \\
	\hline
	5 & -0.06 & -0.07 & 16.7 \\
	\hline
	6.5 & 0.05 & 0.05 & 0.0 \\
	\hline
	7.2 & 0.09 & 0.09 & 0.0 \\
	\hline
\end{tabular}

(Average error: 3.34\%)

\medskip

$\theta_{max} = 0.3$ radians:

\begin{tabular}{|c|c|c|c|}
	\hline
	$t$ [sec] & $\theta_{theory}$ [radians] & $\theta_{measured}$ [radians] & \% error \\
	\hline
	1.5 & 0.11 & 0.11 & 0.0 \\
	\hline
	3 & -0.23 & -0.22 & 4.3 \\
	\hline
	5 & -0.19 & -0.19 & 0.0 \\
	\hline
	6.5 & 0.15 & 0.15 & 0.0 \\
	\hline
	7.2 & 0.27 & 0.27 & 0.0 \\
	\hline
\end{tabular}

(Average error: 0.86\%)

\medskip

$\theta_{max} = 0.7$ radians:

\begin{tabular}{|c|c|c|c|}
	\hline
	$t$ [sec] & $\theta_{theory}$ [radians] & $\theta_{measured}$ [radians] & \% error \\
	\hline
	1.5 & 0.25 & 0.27 & 8.0 \\
	\hline
	3 & -0.53 & -0.49 & 7.5 \\
	\hline
	5 & -0.44 & -0.51 & 15.9 \\
	\hline
	6.5 & 0.36 & 0.26 & 27.8 \\
	\hline
	7.2 & 0.63 & 0.56 & 11.1 \\
	\hline
\end{tabular}

(Average error: 14.06\%)

\medskip

\newpage

$\theta_{max} = 1.2$ radians:

\begin{tabular}{|c|c|c|c|}
	\hline
	$t$ [sec] & $\theta_{theory}$ [radians] & $\theta_{measured}$ [radians] & \% error \\
	\hline
	1.5 & 0.42 & 0.55 & 31.0 \\
	\hline
	3 & -0.90 & -0.73 & 18.9 \\
	\hline
	5 & -0.75 & -1.04 & 38.7 \\
	\hline
	6.5 & 0.62 & 0.09 & 85.5 \\
	\hline
	7.2 & 1.07 & 0.68 & 36.4 \\
	\hline
\end{tabular}

(Average error: 42.1\%)

\subsubsection*{IV. Figures}

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[ht!]
\begin{center}
	\includegraphics[scale=0.8]{lab10_fig1.jpg}
\end{center}
\end{figure}

\newpage

\subsubsection*{V. Analysis}

Several values for the pendulum system need to be calculated to complete our analysis. The theoretical period of the pendulum is $T_{theory} = 2\pi \sqrt{\frac{L}{g}} = 2\pi \sqrt{\frac{15}{9.8}} \approx 1.28 \pi \approx 7.77$ seconds. From this, we can determine that the angular frequency of oscillation is $\omega = \frac{2\pi}{T} \approx 0.81$ radians/second.

The formula $\theta(t) = \theta_{max} \cos(\omega t)$ gives us a theoretical value of $\theta$ for each value of $t$, based on simple harmonic motion. These values of $\theta_{theory}$ for each set of measurements can be found in the data section above. The theoretical values were rounded off to two decimal places before the error percentages were calculated, so as to equal the precision of the measuresments provided by PhysLets. We see that the error percentage increases rapidly as the value of $\theta_{max}$ is increased; the average error is effectively zero (disregarding two measurements with a discrepancy of a hundredth of a radian each) for the first two sets, compared with 42.1\% in the last set (where $\theta_{max} = 1.2$ radians). We know that the formula used to calculate $\theta_{theory}$ is only accurate for small angles; according to the lecture notes for chapter 14 the angle should be no more than $15^{\circ}$, which equals about 0.26 radians. It is no surprise then that the error percentage increased quickly once the angle was higher than 0.3 radians (or $17.2^{\circ}$).

\subsubsection*{VI. Conclusions}

This lab illustrates how the behavior of a simple pendulum becomes more chaotic when the maximum amplitude of its oscillation is larger (in other words, when it is released from a larger angle (and thus a larger height) above its resting position). The equations for simple harmonic motion are no longer reliable for predicting the pendulum's position at this point, and more advanced mathematical tools are required.

\end{flushleft}\end{document}
