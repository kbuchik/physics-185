\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 06 - Springs and Hooke's Law}
\def\mydate{October 23, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}
% Tweak table cell height
\renewcommand{\arraystretch}{0.8}

% Data plot packages
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}

% Lab 6 data
\begin{filecontents*}{lab6_data.csv}
x,F
0,0
2,16
4,32
6,48
8,64
10,80
12,96
14,112
16,128
18,144
20,160
\end{filecontents*}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab uses a PhysLets applet which measures the force exerted by stretching or squishing a simulated spring. We use measurements of this Force along with Hooke's Law to determine the spring constant $k$ associated with the virtual spring.

\subsubsection*{II. Procedures and Descriptions}

PhysLets illustration 5.4 gives us a virtual spring with an unknown spring constant $k$, starting from an equilibrium position of 30 cm. We are able to drag the spring in either direction, stretching or squishing it in increments of 1/6 cm, and the applet will give us the calculated spring force in Newtons. Our experimental procedure directs us to stretch the spring from equilibrium in increments of 2 cm, starting from 0 cm and ending at 20 cm, and record measurements of the change in $x$, and the magnitude of the force $F$ at that position (seen in the table under section III). These measurements are then graphed (with $x$ on the x-axis and $F$ on the y-axis), along with a linear fit of the data points, which can be seen under section IV.

\newpage

\subsubsection*{III. Data}

Equilibrium position: 30 cm

\begin{tabular}{|c|c|}
	\hline
	$x$ [cm] & $F$ [N]
	\csvreader[head to column names]{lab6_data.csv}{}
	{\\\hline\x & \F}
	\\\hline
\end{tabular}

\subsubsection*{IV. Figures}

\begin{tikzpicture}
	% Grid plot setup
	\begin{axis}[xmin = 0, xmax = 20, ymin = 0, ymax = 160, width = \textwidth, height = 0.75\textwidth, xtick distance = 1, ytick distance = 10, grid = both, minor tick num = 1, major grid style = gray!75, xlabel = {Spring extension ($x$) [cm]}, ylabel = {Spring force ($F$) [N]}, legend cell align = {left}, legend pos = north west]
	% Plot data from lab2_data.csv
	\addplot[red, only marks] table[x=x, y=F, col sep=comma]{lab6_data.csv};
	% Draw linear regression
	\addplot[thick, blue] table[x=x, y={create col/linear regression={y=F}}, col sep=comma]{lab6_data.csv};
	% Add legend
	\addlegendentry{\ $x$ vs $F$}
	\addlegendentry{Linear regression: $F = \pgfmathprintnumber{\pgfplotstableregressiona} x$};
\end{axis}
\end{tikzpicture}

\newpage

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[ht!]
\begin{center}
	\includegraphics[scale=1]{lab6_fig1.jpg}
\end{center}
\end{figure}

\newpage

\subsubsection*{V. Analysis}

Recall that Hooke's Law for springs is $F = -kx$. The calculated equation of the linear regression line is $F = 8x$, so the slope is 8 N/cm. Rearranging and multiplying by 100 to convert to meters gives us an estimated value for the spring constant of $k_{experiment} = 800$ N/m. The given theoretical value of the spring constant in this experiment is $k_{theory} = 800$ N/m, which means our error percentage is 0\%.

\subsubsection*{VI. Conclusions}

Because the spring in this experiment is simulated, the data points conform to a perfectly straight line, and using a linear regression is actually redundant because it will provide the same value as calculating the slope directly, and thus it is no surprise that the experimental and theoretical values for $k$ are equal. If the experiment were performed using a real physical spring, the measurements would doubtlessly not be as accurate, and thus the experimental error would be non-zero.

\end{flushleft}
\end{document}
