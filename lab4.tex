\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 04 - Free Fall in Two Dimensions}
\def\mydate{September 30, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}
% Tweak table cell height
\renewcommand{\arraystretch}{0.8}


% Lab 4 data for v0 = 20, h = 0
\begin{filecontents*}{lab4_data1.csv}
initialv,initialdeg,rexp,rtheory,error
20,20,26.4,26.2,0.62
20,30,35.4,35.3,0.15
20,40,40.6,40.2,1.00
20,45,41.2,40.8,0.94
20,50,40.5,40.2,0.76
20,60,35.6,35.3,0.71
20,70,26.3,26.2,0.24
\end{filecontents*}

% Lab 4 data for v0 = 18, h = 5
\begin{filecontents*}{lab4_data2.csv}
initialv,initialdeg,rexp,rtheory,error
18,30,35.9,34.5,4.18
18,40,37.9,36.8,2.92
18,45,37.8,36.7,2.97
18,50,36.5,35.7,2.35
18,60,31.5,30.8,2.26
\end{filecontents*}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

In this lab we use a PhysLets tool to measure the horizontal impact location of a projectile on a ballistic trajectory. Two sets of measurements are taken, each set with a different initial velocity and height, and the initial launch angle varying with each measurement. We then calculate theoretical values for each launch based on its initial parameters, and compare those to our experimental values to calculate error percentages.

\subsubsection*{II. Procedures and Descriptions}

The PhysLets tool under Exploration 3.5 of the Mechanics section allows us to simulate a virtual projectile in free-fall on a ballistic trajectory. This means no forces are acting on it besides gravity, and its predicted landing location can be computed algebraically. We are able to set three parameters for each launch: the initial velocity ($v_0$), the launch angle ($\theta_0$), and the initial height ($y_0$ or $h$). We first run a set of seven launches with initial parameters of $v_0 = 20$ m/s and $h = 0$ m, with a different launch angle each time, increasing from $20^{\circ}$ to $70^{\circ}$. Once the virtual projectile lands after each launch, we record its x-coordinate. This experiment is repeated for a second set of five measurements with initial parameters of $v_0 = 18$ m/s and $h = 5$ m, with launch angle varying from $30^{\circ}$ to $60^{\circ}$.

\newpage

\subsubsection*{III. Data}


Landing position for $\mathbf{v_0 = 20 \ \text{\textbf{m/s}}}$ and $\mathbf{h = 0 \ \text{\textbf{m}}}$:

\begin{tabular}{|c|c|c|c|c|}
	\hline
	$v_0$ [m/s] & $\theta_0$ [degrees] & $R_{exp}$ [m] & $R_{theory}$ [m] & \% error
	\csvreader[head to column names]{lab4_data1.csv}{}
	{\\\hline\initialv & \initialdeg & \rexp & \rtheory & \error}
	\\\hline
\end{tabular}

Average error for first dataset: $0.63\%$

\bigskip

Landing position for $\mathbf{v_0 = 18 \ \text{\textbf{m/s}}}$ and $\mathbf{h = 5 \ \text{\textbf{m}}}$:

\begin{tabular}{|c|c|c|c|c|}
	\hline
	$v_0$ [m/s] & $\theta_0$ [degrees] & $R_{exp}$ [m] & $R_{theory}$ [m] & \% error
	\csvreader[head to column names]{lab4_data2.csv}{}
	{\\\hline\initialv & \initialdeg & \rexp & \rtheory & \error}
	\\\hline
\end{tabular}

Average error for second dataset: $2.93\%$

\newpage

\subsubsection*{IV. Figures}

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[h!]
\begin{center}
	\includegraphics[scale=1]{lab4_fig1.jpg}
\end{center}
\end{figure}

\newpage

\subsubsection*{V. Analysis}

Once our twelve experimental measurements have been taken, we compute and record a corresponding theoretical value for each landing location, based on the initial parameters of the launch. These theoretical values are calculated using the following formula:
\[
	R_{theory} = \frac{v_0^2 \sin(2 \theta_0)}{2g} \left[1 + \sqrt{1 + \frac{2gh}{v_0^2 \sin^2(\theta_0)}}\right]
\]

where $v_0$ is the initial velocity, $h$ is the initial height, $\theta_0$ is the launch angle, and $g$ is obviously the acceleration due to gravity (9.8 m/s).

Finally, we compute error percentages for each measurement, by comparing the experimental value ($R_{exp}$) with the theoretical value ($R_{theory}$), using the following formula:
\[
	\% \ \text{\textit{error}} = \left(\frac{R_{exp} - R_{theory}}{R_{theory}}\right) 100
\]

Once these values have been calculated across the entire dataset, we can see that the average percent error is 0.63\% for the first set of measurements, and 2.93\% for the second set.

\subsubsection*{VI. Conclusions}

My hypothesis for why the second set of measurements had a higher deviation from the results predicted by theory is because the initial height $h$ was zero in the first set, but non-zero in the second set. When $h = 0$, the term under the square root in the formula for $R_{theory}$ reduces to 1, which seems like it reduces the overall complexity of results predicted using the theoretical formula.

\end{flushleft}
\end{document}
