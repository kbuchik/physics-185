\documentclass[12pt]{article}

% Document variables (title, date, author etc)
\def\mytitle{Virtual Lab 09 - Buoyant Force}
\def\mydate{November 20, 2021}
\def\myauthor{Kevin Buchik}
\def\first{Kevin}
\def\last{Buchik}
\def\class{PHYS 185L - 27158}
\def\professor{Arnold Guerra III}

% Margin - 1 inch on all sides
\usepackage[letterpaper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
% Set custom footnote spacing
\setlength{\skip\footins}{0.3in}
% Set custom paragraph spacing
\setlength{\parskip}{1em}
% Double spacing
\usepackage{setspace}
\doublespacing
% Math packages
\usepackage{amsmath, amssymb}
\usepackage{siunitx}
% Graphics support
\usepackage{graphicx}
\graphicspath{{./figures/}}
% CSV file package
\usepackage{csvsimple}

% Page numbering (upper-right corner, last name then page number)
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{\last \ \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
% To make sure we actually have header 0.5in away from top edge
% 12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}
% Correct for headheight warning
\setlength{\headheight}{52pt}

\begin{document}
\begin{flushleft}

\myauthor\\
\class\\
\professor\\
\mydate\\

\begin{center}
\mytitle
\end{center}

\subsubsection*{I. Introduction}

This lab centers around a simulation of a block being progressively lowered by a rope into a tank of water to compare an experimental measurement of the bouyant force (derived from Newton's Second Law based on the tension in the rope), with the theoretical value predicted using Archimedes' Principle (based on the weight of the water displaced by the block).

\subsubsection*{II. Procedures and Descriptions}

PhysLets Exploration 14.2 simulates a rectanglar block (set to a mass of 0.25 kg) hung from above by a rope, which is progressively lowered into a tank of water (with a density set to 700 kg/m$^3$). The simulation tells us the tension force on the rope, and allows us to measure the volume of water displaced by the block into an overflow tank (which has a constant length of 5 cm and a constant depth of 10 cm, but the width (or height) will vary with the amount of water displaced). We begin by recording basic measurements about the volume and weight of the block, then advancing the animation until the block is partially lowered into the water (I paused when it was about halfway submerged), recording the amount of tension in the rope and volume of the displaced liquid. We then resume the animation until the block is fully submerged, and record the measurements a second time. Finally, we calculate an experimental value for the buoyant force (which must equal the tension force according to Newton's second law), and compare it with the theoretical value (which it should equal according to Archimedes' principle) given by the weight of the displaced fluid.

\subsubsection*{III. Data}

\underline{\textbf{Initial Values}}

Length of block: 5 cm

Width of block: 5 cm

Depth of block: 5 cm

Volume of block: $125\ \text{cm}^3 = 1.25 \cdot 10^{-4}\ \text{m}^3$

Mass of block: 0.25 kg

Weight of block: 2.45 N

Fluid density: 700 kg/m$^3$

\underline{\textbf{Block Partially Submerged}}

Tension in cable: 2.0 N

Buoyant force = Weight of block - Tension in cable = $F_{buoyant}^{exp} = 0.45$ N

Length of liquid displaced: 5 cm

Width of liquid displaced: 1.35 cm

Depth of liquid displaced: 10 cm

Volume of liquid displaced: $67.5\ \text{cm}^3 = 6.75 \cdot 10^{-5}\ \text{m}^3$

Mass of liquid displaced: 0.04725 kg

Weight of liquid displaced: $F_{buoyant}^{theory} = 0.463$ N

Error percentage: ${\displaystyle 100\left(\frac{F_{exp} - F_{theory}}{F_{theory}}\right) = -2.8 \%}$

\underline{\textbf{Block Completely Submerged}}

Tension in cable: 1.59 N

Buoyant Force: Weight of block - Tension in cable $= F_{buoyant}^{exp} = 0.86$ N

Length of liquid displaced: 5 cm

Width of liquid displaced: 2.5 cm

Depth of liquid displaced: 10 cm

Volume of liquid displaced: $125\ \text{cm}^3 = 1.25 \cdot 10^{-4}\ \text{m}^3$

Mass of liquid displaced: 0.0875 kg

Weight of liquid displaced: $F_{buoyant}^{theory} = 0.8575$ N

Error percentage: ${\displaystyle 100\left(\frac{F_{exp} - F_{theory}}{F_{theory}}\right) = 0.29 \%}$

\newpage

\subsubsection*{IV. Figures}

A screencap of the experimental interface from PhysLets used in this lab:

\begin{figure}[ht!]
\begin{center}
	\includegraphics[scale=0.8]{lab9_fig1.jpg}
\end{center}
\end{figure}

\subsubsection*{V. Analysis}

When the block is partially (in this case, just about halfway) submerged in the fluid, the tension in the cable is 2 Newtons (in the upward direction). Because the block is stationary, we know from Newton's second law that the net force acting on it must be zero. The downward force acting on the block is its own weight, and so the buoyant force (which acts in the upward direction) must be the difference between the weight of the block (2.45 N) and the tension force, giving us an experimental value for the bouyant force of 0.45 N. The volume of the displaced liquid is 5 cm by 10 cm (both constant values) by 1.35 cm (the dimension which varies as more liquid is displaced), giving us a volume of 67.5 cubic centimeters, or $6.75 \cdot 10^{-5}$ cubic meters. Since the density of the fluid is set to 700 kg/m$^3$, the mass of the displaced fluid is 0.04725 kg, and thus its weight is 0.463 N. By Archimedes' principle, this is our theoretical value for what the buoyant force should be. Plugging these values into our formula for error percentage, we get an error value of -2.8\%.

We repeat these calculations once the block is fully submerged in the fluid. Now, the tension in the cable is 1.59 N, and the experimental value for the buoyant force is 0.86 N. The displaced fluid has a volume of 125 cm$^3$, or  $1.25 \cdot 10^{-4}$ m$^3$, a mass of 0.0875 kg, and a weight (and theoretical value for the buoyant force) of 0.8575 N, which gives us an error percentage of 0.29\%.

\subsubsection*{VI. Conclusions}

This experiment shows that Archimedes' principle is an accurate predictor of the strength of the buoyant force acting on a submerged object. The largest source of the error seen in the experiments was likely caused by inaccuracies in measuring the width of the displaced fluid (since the tool PhysLets uses for measurement requires very precise mouse placement). The lower error percentage in the values for the fully submerged block is also something I'd expect to see in a real life version of this experiment, since no more fluid would be displaced at that point, so measurements of its volume and weight would probably be more accurate than when the block is only partially submerged.

\end{flushleft}
\end{document}
